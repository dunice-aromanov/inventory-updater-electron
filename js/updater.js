$(function() {
    const months = 
            ['jan', 'feb', 'mar', 
            'apr', 'may', 'jun', 'jul', 
            'aug', 'sep', 'oct', 'nov', 'dec'];

    $('<div>Success!</div>').insertBefore('#updateForm');

    $.validator.addMethod(
        "regex",
        function(value, element, regexp) {
            var check = false;
            return this.optional(element) || regexp.test(value);
        },
        "Format is not valid."
    );

    $.validator.addMethod(
        "customDate",
        function(value, element, param) {
            if (param) {
                var regex = /\d{2}[A-Za-z]{3}\d{2}/;
                if (!regex.test(value)) { return false; }
                var valueObj = {
                    date: parseInt(value.slice(0, 2)),
                    month: value.slice(2, 5).toLowerCase(),
                    year: parseInt(value.slice(5, 7))
                }
                return checkDateObj(valueObj);
            } 
            return false;
        },
        "Please, enter valid date (e.g. 01Jun17)"
    );

    $.validator.addMethod(
        "flightsNotEmpty",
        function(value, element, param) {
            if (param) {
                var flights = $('#uflights .flights-container');
                if (flights.length > 0) return true;
                return false;
            } 
            return false;
        },
        "Please, provide one flight at minimum"
    );

    $.validator.addMethod(
        "customPath", 
        function(value, element, param) {
            return /^[A-Za-z]{3}$/.test(value);
        },
        "Format is not valid. (e.g. AOO, NYC)"
    );


    var input = $('#uflights input[type=text]').on('keyup', function(e){
        var key = event.keyCode || event.which;
        if (key === 13) {
            handleFlightAdd($(this), $(this).parent());
            e.preventDefault();
            e.stopPropagation();
            return false;
        }
    });


    $('#updaterForm input[type=text]').blur(function() {
        var value = $(this).val();

        var blackList = ['startDate', 'endDate'];
        var name = $(this).attr('name');
        if ( !(blackList.includes(name)) && value != '') {
            $(this).val(value.toUpperCase());
        }
    });

    $('#updaterSubmit').click(function() {
        $('#updaterForm').submit();
    })

    var validator = $("#updaterForm").validate({
        onkeyup: false,
        rules: {
            flights: {
                required: true
            },
            day: {
                required: true,
                minlength: 1,
            },
            startDate: {
                required: true,
                customDate: true,
                normalizer: function(val) {
                    return $.trim(val);
                }
            },
            endDate: {
                required: true,
                customDate: true,
                normalizer: function(val) {
                    return $.trim(val);
                }
            },
            origin: {
                required: true,
                customPath: true,
                normalizer: function(val) {
                    return $.trim(val);
                }
            },
            destination: {
                required: true,
                customPath: true,
                normalizer: function(val) {
                    return $.trim(val);
                }
            },
            course: {
                required: true
            }
        },
        submitHandler: function(form, e) {
            e.preventDefault();
            var flights = collectFlights();
            if (flights.length == 0) { return; }
            var serializedForm = $(form).serializeArray();
            var commands = makeCommands(serializedForm, flights);
            sendCommands(commands);
        },
        errorPlacement: function(error, el) {
            if (el.attr("name") == "course") {
                error.insertAfter("#courses");
            } else if(el.attr("name") == "day") {
                error.insertAfter("#daysError")
            } else {
                error.insertAfter(el);
            }
        }
    });

    $('#dayAll').change(function(e){
        var val = $(this).val();
        if (val) {
            var daysChecks = collectDaysCheckboxes();
            daysChecks.prop('checked', this.checked);
        }
    });

    $('#dayWeekends').change(function(e){
        var val = $(this).val();
        if (val) {
            var daysChecks = collectWeekendCheckboxes();
            daysChecks.prop('checked', this.checked);
        }
    });

    $('#addFlight').click(function(e){
        e.preventDefault();
        var parent = $(this).parent().parent();
        var input = parent.find('input[type=text]');
        handleFlightAdd(input, parent);
    });

    $('#uflights .flight-input').on('keyup', function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            return false;
        }
    });

    function handleFlightAdd(input, parent) {
        var text = input.val().trim();
        if (isFlightValid(text)) { 
            var errorMessage = $('#uflights [name=flight-adder-error]');
            if (errorMessage.length) {errorMessage.remove();}
            addFlight($('#uflights .flights-container'), `TJ${text}`); 
            input.val('');
        } else { 
            if (!($('div[name=flight-adder-error]').length)) {
                parent.parent().append($('<div>', {
                    text: 'Invalid flight number (e.g. 000, 0000)', 
                    name: 'flight-adder-error', 
                    class: 'updater-error-message error'
                }));
            }
        }
    }

    $('#uflights .flights-container').on('click', 'input[type=button].flight-delete-button', function(e){
        e.preventDefault();
        var parent = $(this).parent();
        parent.remove();
    });

    function normalizeInput(val) {
        return $.trim(val);
    }

    function checkMonth(month) {
        return months.includes(month);
    }

    function checkDay(day) {
        var dateMin = 0,
            dateMax = 31;
        
        if (day >= dateMin && day <= dateMax) { return true; }
        return false; 
    }

    function checkYear(year) {
        var yearMin = 10,
            yearMax = 99;
        if (year >= yearMin && year <= yearMax) { return true; }
        return false; 
    }

    function checkDateObj(obj) {
        if ( checkDay(obj.date) &&
             checkMonth(obj.month) &&
             checkYear(obj.year) ) { return true; }
        return false;
    }

    function collectDaysCheckboxes() {
        return $('#days').find('#dayWeekends, [name=day]');
    }

    function collectWeekendCheckboxes(){
        return $('#daySunday, #daySaturday');
    }

    function collectDays(arr) {
        var days = [];
        for (var obj of arr) {
            if (obj['name'] == 'day') { 
                days.push(obj['value']);
            }
        }
        return days.join("");
    }


    function collectFormData(serializedArray, flight) {
        var formObject = {
            flight: flight,
            origin: $('#uorigin').val().trim(),
            destination: $('#udestination').val().trim(),
            startDate: $('#startDate').val().trim(),
            endDate: $('#endDate').val().trim(),
            days: collectDays(serializedArray),
            inventory: findInventory(serializedArray)
        }
        return formObject;
    }


    function renderSendingString(obj) {
        return `IMN/${obj.flight}/${obj.origin}${obj.destination}/${obj.startDate}/${obj.endDate}/${obj.days}/${obj.inventory}^IMN/${obj.flight}/${obj.origin}${obj.destination}/${obj.startDate}/${obj.endDate}/${obj.days}/U`;
    }


    function findInventory(formArray) {
        for (var item of formArray) {
            if ( item['name'] == 'course' ) { return item['value']; }
        }
        return undefined;
    }


    function renderFlightRecord(flight) {
        return $(renderFlightDiv(flight));
    }

    function renderFlightDiv(flight) {
        return $.
            parseHTML(`
                <div class="flights-item">
                    <div class="flights-item-text">
                        ${flight}
                    </div>
                    <input type="button" class="btn btn-xs btn-default flight-delete-button" data-type="delete-flight" value="x" />
                </div>`
            );
    }

    function addFlight(appendElement, text) {
        var flight = renderFlightRecord(text);
        var el = appendElement.append(flight);
    }


    function isFlightValid(text) {
        var regex = /^\d{1,4}$/;
        return regex.test(text);
    }

    function collectFlights() {
        var flights = $('#uflights .flights-item');
        var result = [];
        flights.each(function(index){
            var text = $(this).find('.flights-item-text').text().trim();
            result.push(text);
        });
        return result;
    }

    function makeCommands(serializedArray, flights){
        var commands = [];
        for (var flight of flights) {
            var formObject = collectFormData(serializedArray, flight);
            var renderedCommand = renderSendingString(formObject);
            commands.push(renderedCommand);
        }
        return commands;
    }

    function removeDuplicateStrings(array) {
       return array.filter(function(elem, index, self) {
            return index == self.indexOf(elem);
        })
    }

    function prepareCommandArray(commands) {
        return commands.join('|%').toUpperCase();
    }

    function sendCommands(commands) {
        var submitButton = $('#updaterForm input[type=submit]').attr('disabled', 'disabled');
        var body = prepareCommandArray(commands);
        $.post('http://ops.xcl.sv:7000/inventory-updater', {
            commands: body
        }, function (data) {
            $('#updaterForm .update-message').show().css('display','inline-block').delay(2000).fadeOut()
            submitButton.removeAttr('disabled');
        })
    }

});